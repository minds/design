# Minds Design

[minds.gitlab.io/design](https://gitlab.minds.io/design).

Minds implementation of [Gitlab Design](https://gitlab.com/gitlab-org/gitlab-design/tree/master).

## Organization

### Naming

Follow these guidelines when naming files and folders, as well as
layers and styles in Sketch:

- Adhere to [BEM naming convention](http://getbem.com/naming/): `block-name__element-name--modifier-name`
- Readability above truncation: `background` instead of `bg`
- `lowercase` everywhere
- Separate words with dashes, `no-spaces`

### Files and folders

```
- hosted/
  - [first-name]/
    - [folders]/
- progress/
    - [gitlabber-first-name]/ (e.g. mark)
        - [product-labels]/ (e.g. settings)
          - projecthandle#issueID-title.sketch (e.g. front#1337-awesome-design.sketch)
          - [projecthandle#issueID-title]/
            - projecthandle#issueID-title--state-one.sketch
            - projecthandle#issueID-title--state-two.sketch
            - assets/
                - asset.svg
- templates/
  - framer/
- minds-instance-sheet.sketch (tbd)
- minds-pattern-library.sketch (tbd)
```

See [Gitlab Design](https://gitlab.com/gitlab-org/gitlab-design/blob/master/CONTRIBUTING.md#files-and-folders) for more information on the structure.

### Work on personal files

For working on your personal files:

1. No need to create a separate branch for your changes. In this project, everything (including personal files) lives on the `master` branch. This is because design files are binary files and do not benefit from the merge request process.
1. In the [`progress`][progress-folder] folder, create your personal folder named after your first name in lowercase (e.g. `pedro`).
1. In your personal folder, create folders and files according to our [organization guidelines](#organization).
1. Make changes while following the [Sketch](#sketch-) and [naming](#naming) guidelines. If you’re working with Sketch specs created with the [Sketch Measure Plugin][sketch-measure], [Framer prototypes][framer], or static HTML pages, please refer to the [Superpowers](#superpowers-) section.
1. Review the files you are about to commit (with `git status -sb`).
   - If you’re having a hard time with this whole Git thing, read our [small help section](#git).
1. Commit and push your changes to `master`, following our [commit guidelines](#commits).
